# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.transaction import Transaction
from trytond.pyson import Eval

from . import invoice

STATES = {
    'readonly': Eval('state') != 'draft',
}

_TYPE = [
    ('out', 'Customer'),
    ('in', 'Supplier'),
]

TYPE_INVOICE_OUT = invoice.TYPE_INVOICE_OUT
TYPE_INVOICE_IN = invoice.TYPE_INVOICE_IN


class InvoiceAuthorization(ModelSQL, ModelView):
    'Invoice Authorization'
    __name__ = 'account.invoice.authorization'
    number = fields.Char('Number Authorization', required=True, states=STATES)
    software_provider_id = fields.Char('Software Provider Id',
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
            'required': Eval('kind').in_(['92', '91', '1', '2', '3', '4']),
        })
    start_date_auth = fields.Date('Start Date Auth', required=True,
        states=STATES)
    end_date_auth = fields.Date('End Date Auth', required=True, states=STATES)
    from_auth = fields.Integer('From Auth', required=True, states=STATES)
    to_auth = fields.Integer('To Auth', required=True, states=STATES)
    sequence = fields.Many2One('ir.sequence.strict', 'Sequence', required=True,
        states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES)
    kind = fields.Selection('get_invoice_type', 'Kind',
        states={
            'required': True,
        })
    state = fields.Selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('finished', 'Finished'),
        ], 'State', select=True)
    kind_string = kind.translated('kind')
    name = fields.Char('Name',  states=STATES)
    type = fields.Selection(_TYPE, 'Type', select=True, required=True,
         states=STATES)

    @classmethod
    def __setup__(cls):
        super(InvoiceAuthorization, cls).__setup__()

    @staticmethod
    def default_type():
        return 'out'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    def get_rec_name(self, name):
        name = self.name or ''
        name = name + ' [' + self.number + ']'
        return name

    @fields.depends('type')
    def get_invoice_type(self):
        types = TYPE_INVOICE_OUT
        if self.type and self.type == 'in':
            types = TYPE_INVOICE_IN 
        return types

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        dom = [bool_op,
            ('number',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ('kind',) + tuple(clause[1:]),
        ]
        return dom
