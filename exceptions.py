# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.exceptions import UserError, UserWarning
# from trytond.model.exceptions import ValidationError


class BadOperationError(UserError):
    pass


class DontWriteAccountError(UserError):
    pass


class MissingInvoiceTaxError(UserError):
    pass


class InvoiceDuplicatedError(UserError):
    pass


class NotificationAuthError(UserError):
    pass


class NotificationAuthWarning(UserWarning):
    pass


class AuthExpiredError(UserError):
    pass


class InvalidTypeInvoiceError(UserError):
    pass


class ImportMoveDataError(UserError):
    pass


class ValidatePartyWarning(UserWarning):
    pass


class ValidatePartyError(UserError):
    pass


class NotificationAdvanceWarning(UserWarning):
    pass
