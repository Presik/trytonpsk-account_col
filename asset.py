from trytond.pool import Pool, PoolMeta
from trytond.model import ModelView, ModelSQL, fields
from dateutil.relativedelta import relativedelta
from trytond.pyson import Eval, Bool


class AssetLocation(ModelView, ModelSQL):
    'Location Asset'
    __name__ = 'account_col.asset_location'

    name = fields.Char('name')
    address = fields.Char('address')


class DepreciationKind(ModelView, ModelSQL):
    'Depreciation Kind'
    __name__ = 'account_col.depreciation_kind'

    name = fields.Char('Name')
    months = fields.Integer('Months Depreciation')
    colgaap = fields.Boolean('Colgaap')

    @staticmethod
    def default_colgaap():
        return False


class Asset(metaclass=PoolMeta):
    __name__ = 'account.asset'

    kind = fields.Many2One('account_col.depreciation_kind', 'Kind',
        required=True, domain=[('colgaap', '=', Eval('colgaap', False))])
    location = fields.Many2One('account_col.asset_location', "Location")
    type = fields.Selection([
        ('AFP', 'Activo fijo propios'),
        ('CD', 'Comodato'),
        ('AC', 'Activo Controlado')
        ], 'Type', required=True)
    asset_colgaap = fields.Many2One('account.asset', "Asset Colgaap",
        domain=[('colgaap', '=', True)], states={
            'invisible': Bool(Eval('colgaap'))}
        )
    code = fields.Char('Code')
    colgaap = fields.Boolean('Colgaap')

    @fields.depends('kind', 'start_date')
    def on_change_start_date(self, name=None):
        if self.kind:
            self.end_date = self.start_date + relativedelta(months=self.kind.months)

    @fields.depends('colgaap')
    def on_change_with_kind(self, name=None):
        return None

    @staticmethod
    def default_colgaap():
        return False

    def get_closing_move(self, account, date=None):
        move = super(Asset, self).get_closing_move(account, date)
        if move:
            for line in move.lines:
                if line.account.party_required:
                    line.party = self.company.party.id
        return move

    def get_move(self, line):
        move = super(Asset, self).get_move(line)
        Company = Pool().get('company.company')
        company = Company.search([])
        account_category = line.asset.product.account_category

        if line.asset.asset_colgaap and move:
            move.method = 'ifrs'
            amount = 0
            debit = 0
            credit = 0
            for l in line.asset.asset_colgaap.lines:
                if line.date == l.date:
                    amount = l.depreciation - line.depreciation

            if amount > 0:
                debit = abs(amount)
            else:
                credit = abs(amount)

            for line in move.lines:
                if line.account.type.fixed_asset:
                    line.credit = credit
                    line.debit = debit
                    line.account = account_category.account_depreciation_diference
                else:
                    line.credit = debit
                    line.debit = credit
                    line.account = account_category.account_expense_diference
                    line.party = self.company.party

        if move:
            for line in move.lines:
                if line.account.type.statement == 'income':
                    line.party = self.company.party
        return move

    @classmethod
    def create_lines(cls, assets):
        for asset in assets:
            if asset.type == 'AC' or asset.type == 'CD':
                continue
            super(Asset, cls).create_lines([asset])
