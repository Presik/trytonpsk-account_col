# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
import time
from datetime import date
from sql import Table, Null, With, Literal
from sql.aggregate import Sum, Aggregate, __all__ as __all__aggregate
from sql.conditionals import Case

from trytond.model import fields, ModelView, ModelSQL
from trytond.pool import Pool
from trytond.modules.company import CompanyReport
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.pyson import Eval
from trytond.exceptions import UserError

_ZERO = Decimal('0.0')
TODAY = date.today()

__all__aggregate.append('ArrayAgg')


class ArrayAgg(Aggregate):
    __slots__ = ()
    _sql = 'ARRAY_AGG'


class Move(ModelSQL, ModelView):
    __name__ = 'account.move'
    balance = fields.Function(fields.Numeric('Balance', digits=(16, 2)),
        'on_change_with_balance')
    method = fields.Selection([
            ('', ''),
            ('ifrs', 'Ifrs'),
            ('colgaap', 'Colgaap'),
            ], 'Method', states={
                'readonly': Eval('state') == 'posted',
        })
    method_string = method.translated('method')

    @fields.depends('lines')
    def on_change_with_balance(self, name=None):
        res = Decimal('0.0')
        for line in self.lines:
            res += (line.debit or 0) - (line.credit or 0)
        return res

    @classmethod
    def draft(cls, records_ids):
        account_move = Table('account_move')
        Line = Pool().get('account.move.line')
        Move = Pool().get('account.move')

        cursor = Transaction().connection.cursor()
        moves = Move.browse(records_ids)
        for move in moves:
            Line.check_journal_period_modify(move.period, move.journal)
        for rec_id in records_ids:
            cursor.execute(*account_move.update(
                columns=[account_move.state],
                values=['draft'],
                where=account_move.id == rec_id)
            )


class Line(ModelSQL, ModelView):
    __name__ = 'account.move.line'
    reference = fields.Char('Reference')
    reconcile_to = fields.Many2One('account.move.line', 'Reconcile To')

    @classmethod
    def __setup__(cls):
        super(Line, cls).__setup__()
        cls._order.insert(0, ('debit', 'DESC'))
        cls._order.insert(1, ('id', 'ASC'))

    @classmethod
    def delete(cls, lines):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        moves = set([x.move for x in lines])
        lines_check = []
        for m in moves:
            lines_check + list(m.lines)
        modified_fields = {'reconciliation', 'account'}
        cls.check_modify(lines, modified_fields=modified_fields)
        cls.check_reconciliation(lines, modified_fields=modified_fields)
        ids = [l.id for l in lines]
        cursor.execute(*sql_table.delete(where=sql_table.id.in_(ids or [None])))

    @classmethod
    def query_get(cls, table):
        '''
        Return SQL clause and fiscal years for account move line
        depending of the context.
        table is the SQL instance of account.move.line table
        '''
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        move = Move.__table__()
        period = Period.__table__()
        fiscalyear = FiscalYear.__table__()
        context = Transaction().context
        company = context.get('company')
        fiscalyear_ids = []
        where = Literal(True)

        if context.get('posted'):
            where &= move.state == 'posted'

        if context.get('colgaap'):
            where &= ((move.method == 'colgaap') | (move.method == Null) | (move.method == ''))
        else:
            where &= ((move.method == 'ifrs') | (move.method == Null) | (move.method == ''))
        if context.get('party'):
            where &= table.party == context.get('party')

        if context.get('journal'):
            where &= move.journal == context['journal']

        date = context.get('date')
        from_date, to_date = context.get('from_date'), context.get('to_date')
        fiscalyear_id = context.get('fiscalyear')
        period_ids = context.get('periods')
        if date:
            fiscalyears = FiscalYear.search([
                    ('start_date', '<=', date),
                    ('end_date', '>=', date),
                    ('company', '=', company),
                    ],
                order=[('start_date', 'DESC')],
                limit=1)
            if fiscalyears:
                fiscalyear_id = fiscalyears[0].id
            else:
                fiscalyear_id = -1
            fiscalyear_ids = list(map(int, fiscalyears))
            where &= period.fiscalyear == fiscalyear_id
            where &= move.date <= date
        elif fiscalyear_id or period_ids is not None or from_date or to_date:
            if fiscalyear_id:
                fiscalyear_ids = [fiscalyear_id]
                where &= fiscalyear.id == fiscalyear_id
            if period_ids is not None:
                where &= move.period.in_(period_ids or [None])
            if from_date:
                where &= move.date >= from_date
            if to_date:
                where &= move.date <= to_date
        else:
            where &= fiscalyear.state == 'open'
            where &= fiscalyear.company == company
            fiscalyears = FiscalYear.search([
                    ('state', '=', 'open'),
                    ('company', '=', company),
                    ])
            fiscalyear_ids = list(map(int, fiscalyears))

        # Use LEFT JOIN to allow database optimization
        # if no joined table is used in the where clause.
        return (table.move.in_(move
                .join(period, 'LEFT', condition=move.period == period.id)
                .join(fiscalyear, 'LEFT',
                    condition=period.fiscalyear == fiscalyear.id)
                .select(move.id, where=where)),
            fiscalyear_ids)


class MoveForceDraft(Wizard):
    'Move Force Draft'
    __name__ = 'account.move.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        id_ = Transaction().context['active_id']
        if id_:
            Move.draft([id_])
        return 'end'


class MoveAutoreconcile(Wizard):
    'Move Autoreconcile'
    __name__ = 'account.move.autoreconcile'
    start_state = 'reconcile'
    reconcile = StateTransition()

    def transition_reconcile(self):
        id_ = Transaction().context['active_id']
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        if id_:
            lines = MoveLine.search([('move', '=', id_)])
            for line in lines:
                if not line.reconcile_to:
                    continue
                MoveLine.reconcile([line, line.reconcile_to])

        return 'end'


class MoveUnreconcile(Wizard):
    'Move Unreconcile'
    __name__ = 'account.move.unreconcile'
    start_state = 'do_unreconcile'
    do_unreconcile = StateTransition()

    def transition_do_unreconcile(self):
        pool = Pool()
        Reconciliation = pool.get('account.move.reconciliation')
        Move = pool.get('account.move')
        id_ = Transaction().context['active_id']
        move, = Move.browse([id_])
        reconciliations = [
            l.reconciliation for l in move.lines if l.reconciliation
        ]
        if reconciliations:
            Reconciliation.delete(reconciliations)
        return 'end'


class AccountMoveSheet(CompanyReport):
    'Account Move Sheet'
    __name__ = 'account.move.sheet'

    @classmethod
    def get_context(cls, records, header, data):
        context = super().get_context(records, header, data)
        for rec in records:
            debits_ = []
            credits_ = []
            for line in rec.lines:
                debits_.append(line.debit)
                credits_.append(line.credit)
            setattr(rec, 'sum_debits', sum(debits_))
            setattr(rec, 'sum_credits', sum(credits_))
        return context


class MoveFixNumberStart(ModelView):
    'Move Fix Number Start'
    __name__ = 'account_col.move.fix_number.start'
    number = fields.Char('New Number', required=True)


class MoveFixNumber(Wizard):
    'Move Fix Number'
    __name__ = 'account.move.fix_number'
    start = StateView(
        'account_col.move.fix_number.start',
        'account_col.move_fix_number_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        account_move = Table('account_move')
        id_ = Transaction().context['active_id']
        cursor = Transaction().connection.cursor()
        if id_:
            cursor.execute(*account_move.update(
                columns=[account_move.number],
                values=[self.start.number],
                where=account_move.id == id_)
            )
        return 'end'


class RenumberMoveStart(ModelView):
    'Renumber Move Start'
    __name__ = 'account_col.renumber_move.start'
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True, domain=[('state', '=', 'open')])
    start_number = fields.Integer('Start Number', required=True)
    journal = fields.Many2One('account.journal', 'Journal',
            required=True)
    start_period = fields.Many2One('account.period', 'Start Period',
        required=True, domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('state', '=', 'open'),
            ('start_date', '<=', (Eval('end_period'), 'start_date'))
            ], depends=['end_period', 'fiscalyear'])
    end_period = fields.Many2One('account.period', 'Start Period',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('state', '=', 'open'),
            ('start_date', '>=', (Eval('start_period'), 'start_date')),
            ], depends=['start_period', 'fiscalyear'])
    company = fields.Many2One('company.company', 'Company',
            required=True)

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('fiscalyear')
    def on_change_fiscalyear(self):
        self.start_period = None
        self.end_period = None


class RenumberMove(Wizard):
    'Renumber Move'
    __name__ = 'account_col.renumber_move'
    start = StateView(
        'account_col.renumber_move.start',
        'account_col.renumber_move_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Journal = pool.get('account.journal')
        Sequence = pool.get('ir.sequence')

        journal, = Journal.browse([self.start.journal])
        start_periods = Period.search([
                ('fiscalyear', '=', self.start.fiscalyear.id),
                ('end_date', '<=', self.start.start_period.start_date),
        ])

        end_periods = []
        if self.start.end_period:
            end_periods = Period.search([
                    ('fiscalyear', '=', self.start.fiscalyear.id),
                    ('end_date', '<=', self.start.end_period.start_date),
                    ])
            end_periods = list(set(end_periods).difference(
                    set(start_periods)))
            if self.start.end_period not in end_periods:
                end_periods.append(self.start.end_period)
        else:
            end_periods = Period.search([
                    ('fiscalyear', '=', self.start.fiscalyear.id),
                    ])
            end_periods = list(set(end_periods).difference(
                    set(start_periods)))

        periods = [p.id for p in end_periods]
        moves = Move.search([
            ('period', 'in', periods),
            ('journal', '=', self.start.journal.id),
        ], order=[('date', 'ASC')])

        cursor = Transaction().connection.cursor()
        Sequence.write([journal.sequence],
                {'number_next': self.start.start_number}
                )
        for move in moves:
            number = Sequence.get_id(journal.sequence.id)
            query = "UPDATE account_move SET number='%s' WHERE id=%s"
            cursor.execute(query % (number, move.id))
        return 'end'


class MoveCloseYearStart(ModelView):
    'Move Close Year Start'
    __name__ = 'account_col.move_close_year.start'
    company = fields.Many2One('company.company', 'Company',
        required=True)
    journal = fields.Many2One('account.journal', 'Journal',
        required=True)
    period = fields.Many2One('account.period', 'Close Period',
        required=True, domain=[
            ('type', '=', 'adjustment'),
        ])
    period_consolidate = fields.Many2One('account.period', 'Consolidate Period',
        required=True, depends=['fiscalyear'], domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True, domain=[
            ('state', '=', 'open'),
        ])
    expense_account = fields.Many2One('account.account',
        'Expense Account', domain=[
            ('type', '!=', None),
        ], required=True)
    description = fields.Char('Description', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class MoveCloseYear(Wizard):
    'Move Close Year'
    __name__ = 'account_col.move_close_year'
    start = StateView(
        'account_col.move_close_year.start',
        'account_col.move_close_year_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'create_', 'tryton-print', default=True),
        ])
    create_ = StateTransition()
    done = StateView(
        'account_col.move_close_year.done',
        'account_col.move_close_year_done_view_form', [
            Button('Done', 'end', 'tryton-ok', default=True),
        ])

    @classmethod
    def __setup__(cls):
        super(MoveCloseYear, cls).__setup__()
        # cls._error_messages.update({
        #     'move_created': 'Move %s created in %s seconds',
        #     'not_moves_period': 'There is not moves in period',
        #     'line_party_required': 'Move Line with account require %s party',
        #     'party_missing': 'Error missing party for account %s in move %s',
        # })

    def transition_create_(self):
        initial_time = time.time()
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Fiscalyear = pool.get('account.fiscalyear')
        Reconciliation = pool.get('account.move.reconciliation')
        Company = pool.get('company.company')
        company = Company(self.start.company)
        fiscalyear = Fiscalyear(self.start.fiscalyear)
        move_line = MoveLine.__table__()
        move_ = Move.__table__()
        account = pool.get('account.account').__table__()
        period = self.start.period_consolidate
        cursor = Transaction().connection.cursor()

        from_ = move_line.join(move_, condition=move_line.move == move_.id
            ).join(account, condition= move_line.account == account.id
            )

        where_ = move_.period == period.id
        where_ &= account.code >= '4'
        where_ &= move_line.reconciliation == Null
        # where_ &= move_.date >= period.start_date
        # where_ &= move_.date <= period.start_date + timedelta(days=5)

        query_1 = from_.select(
            move_.number,
            account.code,
            where=(where_ & (move_line.party == Null))
        )

        cursor.execute(*query_1)
        result1 = cursor.fetchall()

        if result1:
            msg = f'Error missing party for the accounts in the next moves \n {result1}'
            raise UserError(msg)

        columns_ = [
            move_line.account,
            move_line.party,
            Sum(move_line.credit).as_('credit'),
            Sum(move_line.debit).as_('debit'),
            ArrayAgg(move_line.id).as_('ids')
            ]

        withs_ = With(query=from_.select(*columns_, where=where_,
            group_by=[move_line.account, move_line.party]))

        query_3 = withs_.select(withs_.account, withs_.party,
         Case((withs_.credit < withs_.debit, withs_.debit - withs_.credit),else_=0).as_('credit'),
         Case((withs_.credit > withs_.debit, withs_.credit - withs_.debit) ,else_=0).as_('debit'),
         withs_.ids,
         with_=[withs_]
         )

        cursor.execute(*query_3)
        result2 = cursor.fetchall()
        query_4 = withs_.select(Sum(withs_.credit - withs_.debit), with_=[withs_])
        cursor.execute(*query_4)
        balance = cursor.fetchone()[0]
        print(result2, 'total parties-account')
        if not result2:
            raise UserError('No hay movimientos en el periodo')
            return 'done'
        else:
            move, = Move.create([{
                'period': self.start.period.id,
                'date': fiscalyear.end_date,
                'journal': self.start.journal.id,
                'state': 'draft',
                'description': self.start.description,
            }])
            move_id = move.id
            lines_to_create = [{'party': r[1], 'account': r[0], 'credit': r[2], 'debit': r[3], 'state':'valid', 'move': move_id} \
                for r in result2 if (r[2] > 0 or r[3]>0)]
            concile_direct = [r[4] for r in result2 if (r[2]==0 and r[3] == 0)]
            lines_to_concile = {str(r[1]) + '_' + str(r[0]): r[4] for r in result2}

            profit_line = {
                'account': self.start.expense_account.id,
                'move': move_id,
                'debit': _ZERO,
                'credit': _ZERO,
                'party': company.party.id
            }
            if balance > _ZERO:
                profit_line['credit'] = balance
            else:
                profit_line['debit'] = abs(balance)
            start_time = time.time()
            print(start_time - initial_time, 'initial time')
            lines = MoveLine.create(lines_to_create)

            MoveLine.create([profit_line])
            Move.post([move])
            end_time = time.time()
            print(end_time - start_time, 'time post')
            print(balance, 'balance')

            reconciliations = []
            recon_append = reconciliations.append

            for l in lines:
                data_to_concile = lines_to_concile[str(l.party.id) + '_' + str(l.account.id)]

                to_reconcile = data_to_concile + [l.id]
                # print(l.id, data_to_concile, l.party, l.account, l.debit, l.credit)
                conciliation, = Reconciliation.create([{'date': TODAY}])
                cursor.execute(*move_line.update(
                    columns=[move_line.reconciliation],
                    values=[conciliation.id],
                    where=move_line.id.in_(to_reconcile))
                )
            print(concile_direct, 'concile direct')
            for t in concile_direct:
                conciliation, = Reconciliation.create([{'date': TODAY}])
                cursor.execute(*move_line.update(
                    columns=[move_line.reconciliation],
                    values=[conciliation.id],
                    where=move_line.id.in_(t))
                )

            print(time.time() - end_time, 'before time post to final')


        # move_lines = MoveLine.search_read([
        #     ('move.period.fiscalyear', '=', self.start.fiscalyear),
        #     ('move.period.id', '=', self.start.period_consolidate.id),
        #     ('account.code', '>=', '4'),
        #     ('reconciliation', '=', None),
        # ], fields_names=['id', 'account', 'party', 'credit', 'debit',
        #                  'move.number', 'account.code'])

        # if not move_lines:
        #     raise UserError('No hay movimientos en el periodo')
        #     return 'done'

        # move, = Move.create([{
        #     'period': self.start.period.id,
        #     'date': fiscalyear.end_date,
        #     'journal': self.start.journal.id,
        #     'state': 'draft',
        #     'description': self.start.description,
        # }])

        # balance = []
        # lines_to_create = {}
        # to_reconcile = {}
        # move_id = move.id
        # for line in move_lines:
        #     if not line['party']:
        #         code = line['account.']['code']
        #         number = line['move.']['number']
        #         msg = f'Error missing party for account {code} in move {number}'
        #         raise UserError(msg)
        #     if (line['party'], line['account']) not in lines_to_create.keys():
        #         lines_to_create[(line['party'], line['account'])] = {
        #             'account': line['account'],
        #             'party': line['party'],
        #             'debit': [line['credit']],
        #             'credit': [line['debit']],
        #             'description': '',
        #             'state': 'valid',
        #             'move': move_id,
        #         }
        #         to_reconcile[(line['party'], line['account'])] = []
        #     else:
        #         lines_to_create[(line['party'], line['account'])]['debit'].append(line['credit'])
        #         lines_to_create[(line['party'], line['account'])]['credit'].append(line['debit'])

        #     to_reconcile[(line['party'], line['account'])].append(line['id'])
        #     balance.append(line['credit'] - line['debit'])

        # _lines = []
        # for v in lines_to_create.values():
        #     credit = sum(v['credit'])
        #     debit = sum(v['debit'])
        #     v['credit'] = credit
        #     v['debit'] = debit
        #     if credit > 0 and debit > 0:
        #         if credit > debit:
        #             v['credit'] = credit - debit
        #             v['debit'] = 0
        #         else:
        #             v['debit'] = debit - credit
        #             v['credit'] = 0
        #         if credit == debit:
        #             continue
        #     _lines.append(v)

        # lines = MoveLine.create(_lines)

        # profit_line = {
        #     'account': self.start.expense_account.id,
        #     'move': move_id,
        #     'debit': _ZERO,
        #     'credit': _ZERO,
        # }

        # if self.start.expense_account.party_required:
        #     profit_line['party'] = company.party.id
        # balance = sum(balance)
        # if balance > _ZERO:
        #     profit_line['credit'] = balance
        # else:
        #     profit_line['debit'] = abs(balance)

        # MoveLine.create([profit_line])
        # Move.post([move])

        # reconciliations = []

        # for l in lines:
        #     try:
        #         data_to_concile = to_reconcile[(l.party.id, l.account.id)]
        #     except:
        #         return self.raise_user_error('party_missing',
        #             (l.account.code, l.move.number,))
        #     data_to_concile.append(l.id)
        #     reconciliations.append({
        #         'lines': [('add', data_to_concile)],
        #         'date': TODAY,
        #     })

        # Reconciliation.create(reconciliations)
        # end_time = time.time()
        # delta = str(round(end_time - start_time, 2))
        # self.result = self.raise_user_error('move_created',
        #     error_args=(move.number, delta),
        #     raise_exception=False)
        return 'done'


class MoveCloseYearDone(ModelView):
    'Move Close Year Done'
    __name__ = 'account_col.move_close_year.done'
    result = fields.Text('Result', readonly=True)
