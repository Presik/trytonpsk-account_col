# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.exceptions import UserError
from trytond.i18n import gettext


class ProductIdentifier(metaclass=PoolMeta):
    "Product Identifier"
    __name__ = 'product.identifier'

    @classmethod
    def __setup__(cls):
        super(ProductIdentifier, cls).__setup__()
        cls.type.selection.extend([
            ('hs_co', 'Products Tariff Heading CO'),
            ('unspsc', 'United Nations Standard Products and Services Code (UNSPSC)'),
            ])


class PriceList(metaclass=PoolMeta):
    __name__ = 'product.price_list'

    @classmethod
    def delete(cls, records):
        for record in records:
            if record.lines:
                raise UserError(
                    gettext('account_col.msg_dont_delete_price_list'))
        super(PriceList, cls).delete(records)


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    extra_tax = fields.Numeric('Extra Tax', digits=(16, 2))

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()
        cls.description.translate = False

    @classmethod
    def get_amount_with_extra_tax(cls, lines):
        tax_amount = []
        for line in lines:
            if line.product and hasattr(line.product, 'extra_tax') and line.product.extra_tax:
                tax_amount.append(line.product.extra_tax * Decimal(line.quantity))
        return sum(tax_amount)

    def get_rec_name(self, name=None):
        if self.code:
            return '[' + self.code + '] '+self.name
        else:
            return self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        domain = super(Product, cls).search_rec_name(name, clause)
        return domain + [('code', clause[1], clause[2]), ('description', clause[1], clause[2])]


class ProductCategory(metaclass=PoolMeta):
    __name__ = 'product.category'
    account_return_sale = fields.Many2One('account.account',
        'Account Return Sale', domain=[
            [
                'OR',
                ('type.statement', '=', 'income'),
                ('type.debt', '=', True)
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting'])
    account_return_purchase = fields.Many2One('account.account',
        'Account Return Purchase', domain=[
            [
                'OR',
                ('type.statement', '=', 'income'),
                ('type.debt', '=', True)
            ],
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': (~Eval('context', {}).get('company')
                | Eval('account_parent')
                | ~Eval('accounting', False)),
            },
        depends=['account_parent', 'accounting'])
    account_depreciation_diference = fields.Many2One('account.account',
            'Account Depreciation Diference', domain=[
                ('type.fixed_asset', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                    | Eval('account_parent')
                    | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting'])
    account_expense_diference = fields.Many2One('account.account',
            'Account Expense Diference', domain=[
                ('closed', '!=', True),
                ('type.expense', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': (~Eval('context', {}).get('company')
                    | Eval('account_parent')
                    | ~Eval('accounting', False)),
                },
            depends=['account_parent', 'accounting'])


class ProductTemplate(metaclass=PoolMeta):
    __name__ = 'product.template'

    @classmethod
    def __setup__(cls):
        super(ProductTemplate, cls).__setup__()
        cls.name.translate = False
        cls.categories.domain.append(('accounting', '!=', True))
        cls.account_category.states = {
            'required': True,
        }
