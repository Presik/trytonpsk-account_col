from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report


class AuditReportStart(ModelView):
    'Audit Report Start'
    __name__ = 'account_col.audit_report_start'

    company = fields.Many2One('company.company', 'Company')
    start_date = fields.Date('Start Date', states={'required': True})
    end_date = fields.Date('End Date', states={'required': True})

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class AuditReportWizard(Wizard):
    'Audit Report Wizard'
    __name__ = 'account_col.audit_report_wizard'

    start = StateView(
        'account_col.audit_report_start',
        'account_col.print_audit_report_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('account_col.audit_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'company_name': self.start.company.rec_name,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'time_now': self.start.company.time_now()
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AuditReport(Report):
    'Audit Report'
    __name__ = 'account_col.audit_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        start_date = data['start_date']
        end_date = data['end_date']
        Liquidation = pool.get('staff.liquidation')
        Payroll = pool.get('staff.payroll')
        Voucher = pool.get('account.voucher')
        Note = pool.get('account.note')
        Invoice = pool.get('account.invoice')
        Move = pool.get('account.move')
        ShipmentInternal = pool.get('stock.shipment.internal')
        today = pool.get('ir.date').today()

        cursor = Transaction().connection.cursor()

        query = """SELECT m.number, c.code, c.id, c.name FROM account_move_line as ml
                join account_move as m on ml.move=m.id
                join account_account as c on ml.account=c.id where c.type is null"""
        cursor.execute(query)
        columns = list(cursor.description)
        result = cursor.fetchall()
        accounts = {}
        for row in result:
            row_dict = {}
            id_ = None
            for i, col in enumerate(columns):
                if col.name == 'id':
                    id_ = row[i]
                row_dict[col.name] = row[i]
            if id_ in accounts.keys():
                continue
            accounts[id_] = row_dict

        liquidations = Liquidation.search_read([
            ('state', '!=', 'posted'),
            ('liquidation_date', '>=', start_date),
            ('liquidation_date', '<=', end_date),
        ], fields_names=['number', 'liquidation_date', 'employee.rec_name'])

        payrolls = Payroll.search_read([
            ('state', '!=', 'posted'),
            ('date_effective', '>=', start_date),
            ('date_effective', '<=', end_date),
        ], fields_names=['number', 'date_effective', 'employee.rec_name'])

        notes = Note.search_read([
            ('state', '!=', 'posted'),
            ('date', '>=', start_date),
            ('date', '<=', end_date),
        ], fields_names=['number', 'date', 'description'])

        vouchers = Voucher.search_read([
            ('state', '!=', 'posted'),
            ('date', '>=', start_date),
            ('date', '<=', end_date),
        ], fields_names=['number', 'date', 'party.name'])

        invoices = Invoice.search_read([
            ('state', 'not in', ['posted', 'paid']),
            ('invoice_date', '>=', start_date),
            ('invoice_date', '<=', end_date),
        ], fields_names=['number', 'invoice_date', 'party.name'])

        moves = Move.search_read([
            ('state', '!=', 'posted'),
            ('date', '>=', start_date),
            ('date', '<=', end_date),
        ], fields_names=['number', 'date', 'origin.rec_name'])

        moves_future = Move.search_read([
            ('date', '>', today),
        ], fields_names=['number', 'date', 'origin.rec_name'])

        shipments = []
        if 'account_move' in ShipmentInternal._fields:
            shipments = ShipmentInternal.search_read([
                ('account_move', '=', None),
                ('effective_date', '>=', start_date),
                ('effective_date', '<=', end_date),
            ], fields_names=['number', 'effective_date'])

        report_context['liquidations'] = liquidations
        report_context['payrolls'] = payrolls
        report_context['notes'] = notes
        report_context['invoices'] = invoices
        report_context['moves'] = moves
        report_context['vouchers'] = vouchers
        report_context['shipments'] = shipments
        report_context['accounts'] = list(accounts.values())
        report_context['moves_future'] = moves_future
        return report_context
