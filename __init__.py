# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import move
from . import party
from . import company
from . import invoice
from . import account
from . import tax
from . import geographic_codes
from . import product
from . import invoice_authorization
from . import asset
from . import payment_term
from . import user
from . import audit


def register():
    Pool.register(
        geographic_codes.CountryCode,
        geographic_codes.DepartmentCode,
        geographic_codes.CityCode,
        account.BalanceSheetComparisionContext,
        account.BalanceSheetContext,
        account.AccountConfiguration,
        account.CashflowTemplate,
        account.Cashflow,
        account.Account,
        account.Type,
        account.AuxiliaryBookStart,
        account.PrintTrialBalanceDetailedStart,
        account.PrintTrialBalanceStart,
        account.CashflowStatementContext,
        account.PrintBalanceSheetCOLGAAPStart,
        account.OpenChartAccountStart,
        account.PrintIncomeStatementCOLGAAPStart,
        account.PartyWithholdingStart,
        account.AuxiliaryPartyStart,
        account.GeneralLedgerAccountContext,
        account.IncomeStatementContext,
        party.Configuration,
        party.Address,
        party.Party,
        party.PartyFixCodeStart,
        party.Category,
        party.BankAccountNumber,
        tax.TaxLine,
        tax.Tax,
        company.Company,
        asset.Asset,
        asset.AssetLocation,
        asset.DepreciationKind,
        invoice_authorization.InvoiceAuthorization,
        invoice.Invoice,
        invoice.InvoiceLine,
        invoice.InvoiceTax,
        tax.TaxesByInvoiceStart,
        tax.TaxesPostedStart,
        move.MoveFixNumberStart,
        move.Move,
        move.Line,
        move.RenumberMoveStart,
        move.MoveCloseYearStart,
        move.MoveCloseYearDone,
        product.ProductCategory,
        product.ProductTemplate,
        product.Product,
        product.ProductIdentifier,
        product.PriceList,
        invoice.MovesInvoicesStart,
        invoice.InvoiceUpdateStart,
        invoice.InvoiceReport,
        payment_term.PaymentTerm,
        audit.AuditReportStart,
        module='account_col', type_='model')
    Pool.register(
        move.AccountMoveSheet,
        account.AuxiliaryBook,
        account.TrialBalanceDetailed,
        account.TrialBalanceClassic,
        account.BalanceSheet,
        account.IncomeStatement,
        invoice.EquivalentInvoice,
        account.PartyWithholding,
        tax.TaxesByInvoice,
        tax.TaxesPosted,
        account.AuxiliaryParty,
        account.BalanceSheetCOLGAAP,
        account.IncomeStatementCOLGAAP,
        account.CashflowStatement,
        invoice.MovesInvoicesReport,
        audit.AuditReport,
        module='account_col', type_='report')
    Pool.register(
        move.MoveForceDraft,
        move.MoveAutoreconcile,
        move.MoveUnreconcile,
        invoice.InvoiceForceDraft,
        account.PrintAuxiliaryBook,
        account.PrintTrialBalanceDetailed,
        account.PrintTrialBalance,
        move.MoveFixNumber,
        account.PrintPartyWithholding,
        tax.PrintTaxesByInvoice,
        tax.PrintTaxesPosted,
        account.PrintAuxiliaryParty,
        move.RenumberMove,
        account.OpenChartAccount,
        account.PrintBalanceSheetCOLGAAP,
        account.PrintIncomeStatementCOLGAAP,
        move.MoveCloseYear,
        party.PartyFixCode,
        invoice.MovesInvoices,
        invoice.InvoiceUpdate,
        party.PartyFix,
        user.UserLoginAttempRemove,
        audit.AuditReportWizard,
        module='account_col', type_='wizard')
